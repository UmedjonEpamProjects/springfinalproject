-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: finalprojectlibrary
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `issuanceofbooks`
--

DROP TABLE IF EXISTS `issuanceofbooks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issuanceofbooks` (
  `idIssuanceOFbooks` int(11) NOT NULL,
  `book_idBook` int(11) NOT NULL,
  `reader_idReader` int(11) NOT NULL,
  `librarian_idLibrarian` int(11) NOT NULL,
  PRIMARY KEY (`idIssuanceOFbooks`,`book_idBook`,`reader_idReader`,`librarian_idLibrarian`),
  KEY `fk_issuanceofbooks_book_idx` (`book_idBook`),
  KEY `fk_issuanceofbooks_reader1_idx` (`reader_idReader`),
  KEY `fk_issuanceofbooks_librarian1_idx` (`librarian_idLibrarian`),
  CONSTRAINT `fk_issuanceofbooks_book` FOREIGN KEY (`book_idBook`) REFERENCES `book` (`idBook`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_issuanceofbooks_librarian1` FOREIGN KEY (`librarian_idLibrarian`) REFERENCES `librarian` (`idLibrarian`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_issuanceofbooks_reader1` FOREIGN KEY (`reader_idReader`) REFERENCES `reader` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `issuanceofbooks`
--

LOCK TABLES `issuanceofbooks` WRITE;
/*!40000 ALTER TABLE `issuanceofbooks` DISABLE KEYS */;
/*!40000 ALTER TABLE `issuanceofbooks` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-12 22:33:47
