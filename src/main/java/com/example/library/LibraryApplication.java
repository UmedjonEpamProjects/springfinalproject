package com.example.library;

import com.example.library.dto.Reader;
import com.example.library.repository.ReaderRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class LibraryApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(LibraryApplication.class, args);
        //BookRepository bookRepository = (BookRepository) context.getBean("bookRepositoryImpl");
        ReaderRepository readerRepository = (ReaderRepository) context.getBean("readerRepositoryImpl");

        System.out.println("Все читатели");
        readerRepository.getAllReaders().forEach(System.out::println);

        Reader reader = new Reader("Alex",108,"Dushanbe 1");


        long alexId = readerRepository.addReader(reader);
        System.out.println("добавили Alex");

        System.out.println("Все читатели");
        readerRepository.getAllReaders().forEach(System.out::println);

        reader = readerRepository.getReaderByTicket(108);
        reader.setName("Sergey");
        System.out.println("Изменили имя читателя");

        readerRepository.updateReader(reader);
        System.out.println("Обновили читателя");

        System.out.println("Все читатели");
        readerRepository.getAllReaders().forEach(System.out::println);


        readerRepository.removeReader(108);
        System.out.println("Удалили Алекса");

        System.out.println("Все читатели");
        readerRepository.getAllReaders().forEach(System.out::println);


        /**
         bookRepository.addBook(1,"Сказки", "Pushkin",350);
         bookRepository.addBook(2,"Война и Мир","Tolstoy",500);
         bookRepository.addBook(3,"Отцы и Дети", "Turgenov", 400);
         System.out.println(bookRepository.getAllBooks());
         **/

    }
}
