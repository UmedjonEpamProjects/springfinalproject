package com.example.library.mappers;

import com.example.library.dto.Reader;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ReaderMapper implements RowMapper<Reader> {

    @Override
    public Reader mapRow(ResultSet rs, int rowNum) throws SQLException {
        Reader reader = new Reader();
        reader.setId(rs.getInt("Id"));
        reader.setName(rs.getString("Name"));
        reader.setTicket(rs.getInt("Ticket"));
        reader.setCoordinate(rs.getString("Coordinate"));
        return reader;

    }
}
