package com.example.library.repository;

import com.example.library.dto.Reader;

import java.util.List;

public interface ReaderRepository {

    List<Reader> getAllReaders();

    long addReader(Reader reader);

    void updateReader(Reader reader);

    void removeReader(int ticket);

    Reader getReaderByTicket(int ticket);


}
