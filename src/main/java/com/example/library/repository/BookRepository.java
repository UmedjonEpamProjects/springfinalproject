package com.example.library.repository;

import com.example.library.dto.Book;

import java.util.List;

public interface BookRepository {

    Book searchBookTitle(String names);

    Book addBook(int id, String booktitle, String bookAuthor, int price);

    void searchAuthor();

    List<Book> getAllBooks ();

}
