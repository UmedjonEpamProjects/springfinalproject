package com.example.library.repository;

import com.example.library.dto.Book;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class BookRepositoryImpl implements BookRepository {

    public List<Book> books;

    public BookRepositoryImpl(){
        books = new ArrayList();
    }

    @Override
    public Book searchBookTitle(String names) {
        for (int i = 0; i<books.size(); i++){
            if (names.equals(books.get(i).getBooktitle())){
                return books.get(i);
            }
        }
        return null;
    }

    @Override
    public Book addBook(int id, String booktitle, String bookAuthor, int price) {
        Book book = new Book(id, booktitle, bookAuthor, price);
        books.add(book);
        return book;
    }


    @Override
    public void searchAuthor() {

    }
    @Override
    public List<Book> getAllBooks() {
        return books;
    }
}