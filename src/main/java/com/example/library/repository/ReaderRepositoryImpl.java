package com.example.library.repository;

import com.example.library.dto.Reader;
import com.example.library.mappers.ReaderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class ReaderRepositoryImpl implements ReaderRepository {


    @Autowired
    JdbcTemplate jdbcTemplate;
    //private DataSource dataSource;
    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;



    @Override
    public List<Reader> getAllReaders() {
        return jdbcTemplate.query("Select * From reader", new ReaderMapper());
    }

    @Override
    public long addReader(Reader reader) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("name", reader.getName())
                .addValue("coordinate", reader.getCoordinate())
                .addValue("ticket", reader.getTicket());
        namedParameterJdbcTemplate.update("Insert into reader (Name, Coordinate, Ticket) Values (:name,:coordinate,:ticket)", sqlParameterSource, keyHolder);
        return keyHolder.getKey().longValue();
    }

    @Override
    public void updateReader(Reader reader) {
        jdbcTemplate.update("Update reader Set Name = ?, Coordinate = ?, Ticket = ?  Where ID = ?",
                new Object[]{reader.getName(), reader.getCoordinate(), reader.getTicket(), reader.getId()});
    }


    @Override
    public void removeReader(int ticket) {
        jdbcTemplate.update("Delete From reader Where Ticket = ?", ticket);
    }

    @Override
    public Reader getReaderByTicket(int ticket) {
        return jdbcTemplate.queryForObject("Select * From reader Where Ticket = ?", new Object[]{ticket}, new ReaderMapper());
    }

}
