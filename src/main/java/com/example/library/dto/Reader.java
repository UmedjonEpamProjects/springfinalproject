package com.example.library.dto;

public class Reader {

    private int id;
    private String name;
    private int ticket;
    private String coordinate;


    public Reader(){

    }

    public Reader(String name, int ticket, String coordinate) {
        this.name = name;
        this.ticket = ticket;
        this.coordinate = coordinate;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTicket() {
        return ticket;
    }

    public void setTicket(int ticket) {
        this.ticket = ticket;
    }

    public String getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(String coordinate) {
        this.coordinate = coordinate;
    }

    @Override
    public String toString() {
        return "Reader{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", ticket=" + ticket +
                ", coordinate='" + coordinate + '\'' +
                '}';
    }

}
