package com.example.library.dto;

import java.util.ArrayList;

public class Book {
    public Book(int id, String booktitle, String bookAuthor, int price) {
        this.id = id;
        this.booktitle = booktitle;
        this.bookAuthor = bookAuthor;
        this.price = price;
    }

    private int id;

    private String booktitle;

    private String bookAuthor;

    private int price;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBooktitle() {
        return booktitle;
    }

    public void setBooktitle(String booktitle) {
        this.booktitle = booktitle;
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", booktitle='" + booktitle + '\'' +
                ", bookAuthor='" + bookAuthor + '\'' +
                ", price=" + price +
                '}';
    }
}
