package com.example.library.dto;

public class Catalogue {
    private String books;

    public String getBooks() {
        return books;
    }

    public void setBooks(String books) {
        this.books = books;
    }
}
