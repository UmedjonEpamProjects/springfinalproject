package com.example.library.dto;

public class Librarian {

    private String names; //имена

    private String male;  //пол

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getMale() {
        return male;
    }

    public void setMale(String male) {
        this.male = male;
    }
}
